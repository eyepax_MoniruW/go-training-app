package entities

import "go.mongodb.org/mongo-driver/bson/primitive"

type Property struct {
	Id           primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	PropertyName string             `json:"propertyName" bson:"propertyName"`
	PropertyType string             `json:"propertyType" bson:"propertyType"`
	Status       bool               `json:"status" bson:"status"`
	Country      string             `json:"country" bson:"country"`
	City         string             `json:"city" bson:"city"`
}
