package entities

import "go.mongodb.org/mongo-driver/bson/primitive"

type RatePlan struct {
	Id                       primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	RatePlanName             string
	PropertyId               primitive.ObjectID
	Description              string
	RoomTypeRatePlan         RoomTypeRatePlan // embedded document
	Status                   bool
	RateVarianceFromBaseRate float32
	CancellationPolicy       string
	StayPeriodDuration       StayPeriodDuration
	BreakfastInclude         bool
}

type RoomTypeRatePlan struct {
	RoomTypeId           primitive.ObjectID
	ActiveStaus          bool
	RoomTypeRatePlanName string
}

type StayPeriodDuration struct {
	Permanent bool
	StartDate primitive.DateTime
	EndDate   primitive.DateTime
}
