package entities

import "go.mongodb.org/mongo-driver/bson/primitive"

type RoomType struct {
	Id                    primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	PropertyId            primitive.ObjectID
	RoomType              string
	Rooms                 []Room // embedded document
	Status                bool
	MaximumRoomOccupancy  int32
	MaximumNumberOfAdults int32
	MaximumChildAge       int32
	MaximumInfantAge      int32
}

type Room struct {
	RoomNumber   float32
	BookedStatus bool
	ClosedStatus bool
}
