package controllers

import (
	"encoding/json"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http"
)

type BaseController struct {
	Database *mongo.Database
}

func RespondWithError(writer http.ResponseWriter, code int, msg string) {
	RespondWithJson(writer,code, map[string]string{"error":msg})
}

func RespondWithJson(writer http.ResponseWriter, code int, payload interface{}) {
	response,_:= json.Marshal(payload)
	writer.Header().Set("Content-type", "application/json")
	writer.WriteHeader(code)
	writer.Write(response)
}
