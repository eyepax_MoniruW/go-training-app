package controllers

import (
	"../entities"
	"../models"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)

func (baseController BaseController) NewProperty(w http.ResponseWriter, r *http.Request) {
	fmt.Println("New Property")
	collection := baseController.Database.Collection("property")
	propertyModel := models.PropertyModel{
		Collection: collection,
	}
	var property entities.Property
	err := json.NewDecoder(r.Body).Decode(&property)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	} else {
		err3 := propertyModel.CreateProperty(&property)
		if err3 != nil {
			RespondWithError(w, http.StatusBadRequest, err3.Error())
			return
		} else {
			RespondWithJson(w, http.StatusOK, property)
		}
	}
}

func (baseController BaseController) GetPropertyByName(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Get Property By Name")
	collection :=  baseController.Database.Collection("property")
	propertyModel := models.PropertyModel{
		Collection: collection,
	}
	vars := mux.Vars(r)
	id := vars["propertyName"]
	property, err := propertyModel.FindPropertyByName(id)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
	} else {
		RespondWithJson(w, http.StatusOK, property)
	}
}

func (baseController BaseController) GetAllProperties(w http.ResponseWriter, r *http.Request){
	fmt.Println("Get All Properties")
	collection :=baseController.Database.Collection("property")
	propertyModel := models.PropertyModel{
		Collection: collection,
	}
	property, err2 := propertyModel.FindAllProperties()
	if err2 !=nil{
		RespondWithError(w,http.StatusBadRequest,err2.Error())
	} else {
		RespondWithJson(w,http.StatusOK,property)
	}
}
