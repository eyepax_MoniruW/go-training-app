package controllers

import (
	"../entities"
	"../models"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)


func (baseController BaseController) NewRoomType(w http.ResponseWriter, r *http.Request) {
	fmt.Println("New Room Type")
	collection := baseController.Database.Collection("roomType")
	roomTypeModel := models.RoomTypeModel{
		Collection: collection,
	}
	var roomType entities.RoomType
	err := json.NewDecoder(r.Body).Decode(&roomType)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	} else {
		err3 := roomTypeModel.CreateRoomType(&roomType)
		if err3 != nil {
			RespondWithError(w, http.StatusBadRequest, err3.Error())
			return
		} else {
			RespondWithJson(w, http.StatusOK, roomType)
		}
	}
}

func (baseController BaseController) GetRoomTypeByPropertyId(w http.ResponseWriter, r *http.Request) {
	collection := baseController.Database.Collection("roomType")
	roomTypeModel := models.RoomTypeModel{
		Collection: collection,
	}
	vars := mux.Vars(r)
	id := vars["propertyId"]
	roomType, err := roomTypeModel.FindRoomTypeByPropertyId(id)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
	} else {
		RespondWithJson(w, http.StatusOK, roomType)
	}
}

func (baseController BaseController) GetAllRoomTypes(w http.ResponseWriter, r *http.Request){
	fmt.Println("Get All Room Types")
	collection :=baseController.Database.Collection("roomType")
	roomTypeModel := models.RoomTypeModel{
		Collection: collection,
	}
	allRoomTypes, err2 := roomTypeModel.FindAllRoomTypes()
	if err2 !=nil{
		RespondWithError(w,http.StatusBadRequest,err2.Error())
	} else {
		RespondWithJson(w,http.StatusOK,allRoomTypes)
	}
}
