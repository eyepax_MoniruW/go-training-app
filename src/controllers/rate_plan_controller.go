package controllers

import (
	"../entities"
	"../models"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson/primitive"

	//"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
)

func (baseController BaseController)NewRatePlan(w http.ResponseWriter, r *http.Request){
	fmt.Println("New Rate Plan")
	collection :=baseController.Database.Collection("ratePlan")
	ratePlanModel := models.RatePlanModel{
		Collection: collection,
	}

	var ratePlan entities.RatePlan
	err := json.NewDecoder(r.Body).Decode(&ratePlan)
	if err != nil {
		RespondWithError(w,http.StatusBadRequest,err.Error())
		return
	} else {
		//fmt.Printf("%+v\n",ratePlan)
		err3 :=ratePlanModel.CreateRatePlan(&ratePlan)
		if err3 != nil {
			RespondWithError(w,http.StatusBadRequest,err3.Error())
			return
		} else {
			RespondWithJson(w,http.StatusOK,ratePlan)
		}
	}
}

func (baseController BaseController)GetRatePlanByPropertyId (w http.ResponseWriter, r *http.Request)  {
	fmt.Println("Get Rate Plan By Propery ID")
	collection :=baseController.Database.Collection("ratePlan")
	ratePlanModel := models.RatePlanModel{
		Collection: collection,
	}
	vars :=mux.Vars(r)
	id,_:=primitive.ObjectIDFromHex(vars["propertyId"])
	ratePlan, err := ratePlanModel.FindRatePlanByPropertyId(id)
	if err !=nil{
		RespondWithError(w,http.StatusBadRequest,err.Error())
	} else {
		RespondWithJson(w,http.StatusOK,ratePlan)
	}
}

func (baseController BaseController) GetAllRatePlans(w http.ResponseWriter, r *http.Request){
	fmt.Println("Get All Rate Plans")
	collection :=baseController.Database.Collection("ratePlan")
	ratePlanModel := models.RatePlanModel{
		Collection: collection,
	}
	ratePlans, err2 := ratePlanModel.FindAllRatePlans()
	if err2 !=nil{
		RespondWithError(w,http.StatusBadRequest,err2.Error())
	} else {
		RespondWithJson(w,http.StatusOK,ratePlans)
	}
}