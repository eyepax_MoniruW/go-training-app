package models

import (
	"../entities"
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type RoomTypeModel struct {
	Collection *mongo.Collection
}

func (roomTypeModel RoomTypeModel) CreateRoomType(roomType *entities.RoomType) error {
	insertResult, err := roomTypeModel.Collection.InsertOne(context.TODO(), &roomType)
	fmt.Println("Inserted a single document: ", insertResult.InsertedID)
	return err
}

func (roomTypeModel RoomTypeModel) FindRoomTypeByPropertyId(propertyId string) (roomType entities.RoomType, err error) {
	filter := bson.M{"propertyid": propertyId}
	//filter := bson.D{{"propertyid","100"}}
	err = roomTypeModel.Collection.FindOne(context.TODO(), filter).Decode(&roomType)
	return
}

func (roomTypeModel RoomTypeModel) FindAllRoomTypes() ([]entities.RoomType, error) {
	var allRoomType [] entities.RoomType
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	cursor, err := roomTypeModel.Collection.Find(context.TODO(), bson.M{})
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var roomType entities.RoomType
		cursor.Decode(&roomType)
		allRoomType = append(allRoomType, roomType)
	}
	return allRoomType, err
}
