package models

import (
	"../entities"
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type PropertyModel struct {
	Collection *mongo.Collection
}

func (propertyModel PropertyModel) CreateProperty(property *entities.Property) error {
	insertResult, err := propertyModel.Collection.InsertOne(context.TODO(), &property)
	fmt.Println("Inserted a property. Property ID: ", insertResult.InsertedID)
	return err
}

func (propertyModel PropertyModel) FindPropertyByName(propertyName string) (property entities.Property, err error) {
	//filter := bson.M{"propertyName": propertyName}
	filter := bson.D{{"propertyName", propertyName}}
	err = propertyModel.Collection.FindOne(context.TODO(), filter).Decode(&property)
	return
}

func (propertyModel PropertyModel) FindAllProperties() ([]entities.Property, error) {
	var properties [] entities.Property
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	cursor, err := propertyModel.Collection.Find(context.TODO(), bson.M{})
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var property entities.Property
		cursor.Decode(&property)
		properties = append(properties, property)
	}
	return properties, err
}
