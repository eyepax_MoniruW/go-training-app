package models

import (
	"../entities"
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type RatePlanModel struct {
	Collection *mongo.Collection
}

func (ratePlanModel RatePlanModel) CreateRatePlan(ratePlan *entities.RatePlan) error {
	insertResult, err := ratePlanModel.Collection.InsertOne(context.TODO(), &ratePlan)
	fmt.Println("Inserted a single document: ", insertResult.InsertedID)
	return err
}

func (ratePlanModel RatePlanModel) FindRatePlanByPropertyId(propertyId primitive.ObjectID) (ratePlan entities.RatePlan, err error) {
	//filter := bson.M{"propertyid": propertyId}
	filter := bson.D{{"propertyid",propertyId}}
	fmt.Println(propertyId)
	err = ratePlanModel.Collection.FindOne(context.TODO(), filter).Decode(&ratePlan)
	return
}

func (ratePlanModel RatePlanModel) FindAllRatePlans() ([]entities.RatePlan, error) {
	var ratePlans [] entities.RatePlan
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	cursor, err := ratePlanModel.Collection.Find(context.TODO(), bson.M{})
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var ratePlan entities.RatePlan
		cursor.Decode(&ratePlan)
		ratePlans = append(ratePlans, ratePlan)
	}
	return ratePlans, err
}
