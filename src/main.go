package main

import (
	"./config"
	"./controllers"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func main() {
	router := mux.NewRouter()
	fmt.Println("Application Started")

	dbClient, clientError := config.GetMongoDB()

	if clientError != nil {
		fmt.Printf("Error Connecting to DB. Error :- %s\n", clientError.Error())
		return
	} else {
		propertyController := controllers.BaseController{Database: dbClient.Database("goTrainingApp")}

		// Property End Points
		router.HandleFunc("/properties", propertyController.GetAllProperties).Methods("GET")
		router.HandleFunc("/property/{propertyName}", propertyController.GetPropertyByName).Methods("GET")
		router.HandleFunc("/property", propertyController.NewProperty).Methods("POST")

		// Rate Plan End Points
		router.HandleFunc("/rateplans", propertyController.GetAllRatePlans).Methods("GET")
		router.HandleFunc("/property/{propertyId}/rateplans", propertyController.GetRatePlanByPropertyId).Methods("GET")
		router.HandleFunc("/rateplan", propertyController.NewRatePlan).Methods("POST")

		// Room Type End Points
		router.HandleFunc("/roomtypes", propertyController.GetAllRoomTypes).Methods("GET")
		router.HandleFunc("/property/{propertyId}/roomtype", propertyController.GetRoomTypeByPropertyId).Methods("GET")
		router.HandleFunc("/roomtype", propertyController.NewRoomType).Methods("POST")

		log.Fatal(http.ListenAndServe(":8000", router))
	}
}
